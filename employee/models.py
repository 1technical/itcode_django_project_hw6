from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.urls import reverse


class Employee(models.Model):
    first_name = models.CharField(max_length=255, verbose_name="Имя")
    last_name = models.CharField(max_length=255, verbose_name="Фамилия")
    job_title = models.CharField(max_length=255, verbose_name="Должность")
    address = models.CharField(max_length=255, verbose_name="Адрес")
    email = models.EmailField(max_length=255, null=True)
    phone = models.PositiveIntegerField()
    slug = models.SlugField(max_length=255, unique=True, db_index=True, verbose_name="URL")
    kpi = models.IntegerField(
        default=50,
        validators=[MaxValueValidator(150), MinValueValidator(50)], verbose_name="KPI"
    )
    time_create = models.DateTimeField(auto_now_add=True, verbose_name="Время создания")

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

    def get_absolute_url(self):
        return reverse('showcard', kwargs={'card_slug': self.slug})

    class Meta:
        verbose_name = 'Сотрудник'
        verbose_name_plural = 'Сотрудники'
        ordering = ('-kpi', 'id')
