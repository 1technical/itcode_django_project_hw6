# Generated by Django 4.2 on 2023-04-30 06:19

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=255, verbose_name='Имя')),
                ('last_name', models.CharField(max_length=255, verbose_name='Фамилия')),
                ('job_title', models.CharField(max_length=255, verbose_name='Должность')),
                ('address', models.CharField(max_length=255, verbose_name='Адрес')),
                ('email', models.EmailField(max_length=255, null=True)),
                ('phone', models.PositiveIntegerField()),
                ('slug', models.SlugField(max_length=255, unique=True, verbose_name='URL')),
                ('kpi', models.IntegerField(default=50, validators=[django.core.validators.MaxValueValidator(150), django.core.validators.MinValueValidator(1)], verbose_name='KPI')),
                ('time_create', models.DateTimeField(auto_now_add=True, verbose_name='Время создания')),
            ],
            options={
                'verbose_name': 'Сотрудник',
                'verbose_name_plural': 'Сотрудники',
                'ordering': ('-kpi', 'id'),
            },
        ),
    ]
