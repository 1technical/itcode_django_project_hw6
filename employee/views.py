from django.core.paginator import Paginator
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from slugify import slugify

from employee.forms import AddEmployeeForm
from employee.models import Employee

menu = [{'title': 'Список сотрудников', 'url_name': 'employee_list'},
        {'title': 'Сотрудник месяца', 'url_name': 'employeofthemonth'},
        {'title': 'Добавить сотрудника', 'url_name': 'addform'},
        ]


def index(request):
    context = {
        'menu': menu,
        'title': 'Главная страница'
    }
    return render(request, 'employee/index.html', context=context)


def employee_list(request):
    employeelist = Employee.objects.all()
    paginator = Paginator(employeelist, 10)
    context = {
        'employee': employeelist,
        'menu': menu,
        'title': 'Список сотрудников'
    }
    return render(request, 'employee/employee_list.html', context=context)


def employeofthemonth(request):
    employee = Employee.objects.order_by('-kpi')
    context = {
        'employee': employee,
        'menu': menu,
        'title': 'Сотрудник Месяца'
    }
    return render(request, 'employee/employeofthemonth.html', context=context)


def showcard(request, card_slug):
    card = get_object_or_404(Employee, slug=card_slug)
    employee = Employee.objects.filter(slug=card_slug)
    context = {
        'employee': employee,
        'card': card,
        'menu': menu,
        'title': 'Карточка сотрудника'
    }
    return render(request, 'employee/showcard.html', context=context)


def addform(request):
    form = AddEmployeeForm(request.POST)
    context = {'form': form,
               'menu': menu,
               'title': 'Добавить Сотрудника'
               }
    if form.is_valid():
        worker = form.save(False)
        worker.slug = slugify(worker.first_name + '-' + worker.last_name)
        worker.save()
        return HttpResponseRedirect('employee_list')
    return render(request, 'employee/addform.html', context=context)
