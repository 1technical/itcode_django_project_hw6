from django.contrib import admin
from employee.models import Employee


class EmployeeAdmin(admin.ModelAdmin):
    list_display = ('id', 'first_name', 'last_name', 'job_title', 'address', 'email', 'phone', 'kpi', 'time_create')
    list_display_links = ('first_name',)
    search_fields = ('first_name', 'last_name')
    list_filter = ('time_create', 'kpi',)
    readonly_fields = ('time_create',)
    prepopulated_fields = {'slug': ('first_name', 'last_name')}


admin.site.register(Employee, EmployeeAdmin)
