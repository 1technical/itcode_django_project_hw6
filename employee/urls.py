from django.urls import path
from .views import *

urlpatterns = [
    path('', index, name='index'),
    path('employee_list', employee_list, name='employee_list'),
    path('employeofthemonth', employeofthemonth, name='employeofthemonth'),
    path('addform', addform, name='addform'),
    path('showcard/<slug:card_slug>/', showcard, name='showcard'),
]